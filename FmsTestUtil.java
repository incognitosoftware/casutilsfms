package com.incognito.utils.fms.security;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.incognito.eco.cas.restclient.CasRestClient;
import com.incognito.eco.cas.restvo.LoginRequest;
import com.incognito.eco.cas.restvo.UserLoginResponse;
import com.incognito.general.util.StringUtils;
import com.incognito.restclient.RestManager;

public class FmsTestUtil {

	public static FmsRestClient connect(String fmsHost, int port) {
		if (StringUtils.isBlank(fmsHost)) {
			throw new RuntimeException("casHost is not configured");
		}

		List<URI> uris = new ArrayList<>();
		try {
			uris.add(new URI("http://" + fmsHost + ":" + port));
		} catch (URISyntaxException e) {
			throw new RuntimeException("Failed to resolve the URI for cas");
		}

		RestManager.setReadTimeout(1000000); // make time out long for debugging
		RestManager manager = new RestManager((String) null, true, uris);

		return new FmsRestClient(manager);
	}

	/**
	 * Login the client with user name and password via CAS
	 * 
	 * @param client
	 * @param casHost
	 * @param userName
	 * @param password
	 * @return
	 */
	public static UserLoginResponse login(FmsRestClient client, String casHost, String userName, String password) {
		CasRestClient casRC = CasTestUtil.connectPublic(casHost);
		LoginRequest loginVo = new LoginRequest();
		loginVo.setUsername(userName);
		loginVo.setPassword(password);
		UserLoginResponse loginResp = casRC.login(loginVo);
		client.getManager().setAuthToken(loginResp.getAuthorization());
		client.getManager().setStoreStatus(true);
		return loginResp;
	}

	/**
	 * Logout the client via CAS
	 * 
	 * @param client
	 * @param casHost
	 */
	public static void logout(FmsRestClient client, String casHost) {
		CasRestClient casRC = CasTestUtil.connectPublic(casHost);
		casRC.getManager().setAuthToken(client.getManager().getAuthToken());
		casRC.logout();
	}

}
