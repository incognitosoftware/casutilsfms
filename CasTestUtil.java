package com.incognito.utils.fms.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import com.incognito.eco.cas.restclient.CasRestClient;
import com.incognito.eco.cas.restvo.LoginRequest;
import com.incognito.eco.cas.restvo.Policy;
import com.incognito.eco.cas.restvo.UserGroup;
import com.incognito.eco.cas.restvo.User;
import com.incognito.eco.cas.restvo.UserLoginResponse;
import com.incognito.general.util.StringUtils;
import com.incognito.restclient.RestManager;
import com.incognito.restclient.exceptions.RestException;

public class CasTestUtil {
	// Default ports -- CAS-255
	private final static int API_PUBLIC_PORT = 3141;
	private final static int API_PRIVATE_PORT = 3140;

	public static CasRestClient connectPublic(String casHost) {
		return connect(casHost, API_PUBLIC_PORT);
	}

	public static CasRestClient connectPrivate(String casHost) {
		return connect(casHost, API_PRIVATE_PORT);
	}

	public static CasRestClient connect(String casHost, int port) {
		if (StringUtils.isBlank(casHost)) {
			throw new RuntimeException("casHost is not configured");
		}

		List<URI> uris = new ArrayList<>();
		try {
			uris.add(new URI("https://" + casHost + ":" + port));
		} catch (URISyntaxException e) {
			throw new RuntimeException("Failed to resolve the URI for cas");
		}

		RestManager.setReadTimeout(1000000); // make time out long for debugging
		RestManager manager = new RestManager((String) null, true, uris);

		return new CasRestClient(manager);
	}

	/**
	 * Login the client with user name and password
	 * 
	 * @param client
	 * @param userName
	 * @param password
	 * @return
	 */
	public static UserLoginResponse login(CasRestClient client, String userName, String password) {
		LoginRequest loginVo = new LoginRequest();
		loginVo.setUsername(userName);
		loginVo.setPassword(password);
		UserLoginResponse loginResp = client.login(loginVo);
		client.getManager().setAuthToken(loginResp.getAuthorization());
		client.getManager().setStoreStatus(true);
		return loginResp;
	}

	// --------------- utils for User -------------------
	public static User generateCreateUser(String username) {
		User createUser = new User();
		createUser.setUsername(username);
		createUser.setFirstName(" Software System ");
		createUser.setLastName(" Incognito ");
		createUser.setAuthSource("LOCAL");
		createUser.setEmail(" system@incognito.com ");
		createUser.setType("USER");
		createUser.setPassword("testing");
		return createUser;
	}

	public static User validUserCreation(CasRestClient client, User createUser, String errorMessage) {
		User createdUser = null;
		try {
			createdUser = client.createUser(createUser);
		} catch (RestException re) {
			fail(errorMessage + ": failed to create user with error code: " + re.getErrorCode() + ", and message: "
					+ re.getLocalizedMessage());
		}

		// check created expected
		assertNotNull("User missing id.", createdUser.getId());
		// id should be ignored
		createUser.setId(null);
		assertTrue("Created user not the same.", createUser.equalUser(createdUser));
		User retrievedUser = client.getUser(createdUser.getId());
		assertTrue("Retrieved user not the same.", retrievedUser.equalUser(createdUser));

		return createdUser;
	}

	public static void invalidUserCreation(CasRestClient client, User createUser, int expectedCode,
			String errorMessage) {
		try {
			client.createUser(createUser);
			fail(errorMessage + ": Should fail to create user with error code: " + expectedCode);
		} catch (RestException re) {
			assertEquals(errorMessage + " " + re.getLocalizedMessage() + ": Error code not expected: ", expectedCode,
					re.getErrorCode());
		}
	}

	public static User validUserUpdate(CasRestClient client, User updateUser, String errorMessage) {
		User updatedUser = null;
		try {
			updatedUser = client.updateUser(updateUser);
		} catch (RestException re) {
			if (updateUser.getUsername() == null) {
				/*
				 * username is optional for updating User, using errorMessage to
				 * store original username.
				 */
				errorMessage = "CAS-265";
			}
			fail(errorMessage + ": failed to update user with error code: " + re.getErrorCode() + ", and message: "
					+ re.getLocalizedMessage());
		}

		if (updateUser.getUsername() == null || updateUser.getUsername().isEmpty()) {
			/*
			 * username is optional for updating User, using errorMessage to
			 * store original username
			 */
			updateUser.setUsername(errorMessage);
		}
		// check updated expected
		assertTrue("CAS-241: updated user not the same.", updateUser.equalUser(updatedUser));
		User retrievedUser = client.getUser(updateUser.getId());
		assertTrue("Retrieved user not the same.", updateUser.equalUser(retrievedUser));

		return updatedUser;
	}

	public static void invalidUserUpdate(CasRestClient client, User updateUser, int expectedCode, String errorMessage) {
		try {
			client.updateUser(updateUser);
			fail(errorMessage + ": Should fail to update user with error code: " + expectedCode);
		} catch (RestException re) {
			assertEquals(errorMessage + " " + re.getLocalizedMessage() + ": Error code not expected: ", expectedCode,
					re.getErrorCode());
		}
	}

	// --------------- utils for User Group---------------
	public static UserGroup generateCreateUserGroup(String userGroupName) {
		UserGroup createUserGroup = new UserGroup();
		createUserGroup.setName(userGroupName);
		createUserGroup.setDescription("Description of " + userGroupName);
		return createUserGroup;
	}

	public static UserGroup validUserGroupCreation(CasRestClient client, UserGroup createUserGroup,
			String errorMessage) {
		UserGroup createdUserGroup = null;
		try {
			createdUserGroup = client.createUserGroup(createUserGroup);
		} catch (RestException re) {
			fail(errorMessage + ": failed to create user group with error code: " + re.getErrorCode()
					+ ", and message: " + re.getLocalizedMessage());
		}

		// check created expected
		assertNotNull("UserGroup missing id.", createdUserGroup.getId());
		// id should be ignored
		createUserGroup.setId(null);
		assertTrue("Created user group not the same.", createUserGroup.equalUserGroup(createdUserGroup));
		UserGroup retrievedUserGroup = client.getUserGroup(createdUserGroup.getId());
		assertTrue("CAS-282: Retrieved user group not the same.", retrievedUserGroup.equalUserGroup(createdUserGroup));

		return createdUserGroup;
	}

	public static void invalidUserGroupCreation(CasRestClient client, UserGroup createUserGroup, int expectedCode,
			String errorMessage) {
		try {
			client.createUserGroup(createUserGroup);
			fail(errorMessage + ": Should fail to create user group with error code: " + expectedCode);
		} catch (RestException re) {
			assertEquals(errorMessage + " " + re.getLocalizedMessage() + ": Error code not expected: ", expectedCode,
					re.getErrorCode());
		}
	}

	public static UserGroup validUserGroupUpdate(CasRestClient client, UserGroup updateUserGroup, String errorMessage) {
		UserGroup updatedUserGroup = null;
		try {
			updatedUserGroup = client.updateUserGroup(updateUserGroup);
		} catch (RestException re) {
			fail(errorMessage + ": failed to update user group with error code: " + re.getErrorCode()
					+ ", and message: " + re.getLocalizedMessage());
		}

		// check updated expected
		assertTrue("CAS-241: Updated user group not the same.", updateUserGroup.equalUserGroup(updatedUserGroup));
		UserGroup retrievedUserGroup = client.getUserGroup(updateUserGroup.getId());
		assertTrue("Retrieved user group not the same.", updateUserGroup.equalUserGroup(retrievedUserGroup));

		return updatedUserGroup;
	}

	public static void invalidUserGroupUpdate(CasRestClient client, UserGroup updateUserGroup, int expectedCode,
			String errorMessage) {
		try {
			client.updateUserGroup(updateUserGroup);
			fail(errorMessage + ": Should fail to update user group with error code: " + expectedCode);
		} catch (RestException re) {
			assertEquals(errorMessage + " " + re.getLocalizedMessage() + ": Error code not expected: ", expectedCode,
					re.getErrorCode());
		}
	}

	// -------------- utils for Policy--------------------
	public static Policy generateCreatePolicy(String policyName) {
		Policy createPolicy = new Policy();
		createPolicy.setName(policyName);
		createPolicy.setDescription("Description of " + policyName);
		createPolicy.setServiceType("service type " + policyName);
		List<String> serverNames = new ArrayList<>();
		serverNames.add("Server " + policyName);
		// createPolicy.setServerNames(serverNames);
		return createPolicy;
	}

	public static Policy validPolicyCreation(CasRestClient client, Policy createPolicy, String errorMessage) {
		Policy createdPolicy = null;
		try {
			createdPolicy = client.createPolicy(createPolicy);
		} catch (RestException re) {
			fail(errorMessage + ": failed to create policy with error code: " + re.getErrorCode() + ", and message: "
					+ re.getLocalizedMessage());
		}

		// check created expected
		assertNotNull("Policy missing id.", createdPolicy.getId());
		// id should be ignored
		createPolicy.setId(null);
		assertTrue("Created policy not the same.", createPolicy.equalPolicy(createdPolicy));
		Policy retrievedPolicy = client.getPolicy(createdPolicy.getId());
		assertTrue("Retrieved policy not the same.", retrievedPolicy.equalPolicy(createdPolicy));

		return createdPolicy;
	}

	public static void invalidPolicyCreation(CasRestClient client, Policy createPolicy, int expectedCode,
			String errorMessage) {
		try {
			client.createPolicy(createPolicy);
			fail(errorMessage + ": Should fail to create policy with error code: " + expectedCode);
		} catch (RestException re) {
			assertEquals(errorMessage + " " + re.getLocalizedMessage() + ": Error code not expected: ", expectedCode,
					re.getErrorCode());
		}
	}

	public static Policy validPolicyUpdate(CasRestClient client, Policy updatePolicy, String errorMessage) {
		Policy updatedPolicy = null;
		try {
			updatedPolicy = client.updatePolicy(updatePolicy);
		} catch (RestException re) {
			fail(errorMessage + ": failed to update policy with error code: " + re.getErrorCode() + ", and message: "
					+ re.getLocalizedMessage());
		}

		// check updated expected
		assertTrue("CAS-241: Created policy not the same.", updatePolicy.equalPolicy(updatedPolicy));
		Policy retrievedPolicy = client.getPolicy(updatePolicy.getId());
		assertTrue("Retrieved policy not the same.", updatePolicy.equalPolicy(retrievedPolicy));

		return updatedPolicy;
	}

	public static void invalidPolicyUpdate(CasRestClient client, Policy updatePolicy, int expectedCode,
			String errorMessage) {
		try {
			client.updatePolicy(updatePolicy);
			fail(errorMessage + ": Should fail to update policy with error code: " + expectedCode);
		} catch (RestException re) {
			assertEquals(errorMessage + " " + re.getLocalizedMessage() + ": Error code not expected: ", expectedCode,
					re.getErrorCode());
		}
	}

}
