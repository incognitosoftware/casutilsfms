package com.incognito.utils.fms.security;

import com.incognito.eco.cas.restclient.CasRestClient;
import com.incognito.eco.cas.restvo.Policy;
import com.incognito.eco.cas.restvo.User;
import com.incognito.eco.cas.restvo.UserGroup;
import com.incognito.eco.cas.restvo.common.Statement;
import com.incognito.eco.cas.restvo.common.Statement.Effect;
import com.incognito.eco.cas.restvo.common.Summary;
import com.incognito.rest.common.vo.RestSearchResultVo;
import com.incognito.utils.fms.log.Log;

import java.util.ArrayList;
import java.util.List;

public class PrepareCASObjects {
	protected static String PREFIX = "FMSTest";

	private EcoTestManager m_ecoTestManager = EcoTestManager.getEcoTestManager();
	private CasRestClient m_casRestClient = m_ecoTestManager.getCasRestClient();

	private static Policy allowRegionPolicy, denyRegionCmtsPolicy, allAccessPolicy, readOnlyPolicy;
	private static UserGroup allowPoliciesUserGroup, denyPoliciesUserGroup, noPoliciesUserGroup, allAccessUserGroup;
	@SuppressWarnings("unused")
	private static UserGroup readOnlyUserGroup; // for manual assistant test
	private static User permissionUser, noPermissionUser;

	private String serverName = null;

	public static User getPermissionUser() {
		return permissionUser;
	}

	public static User getNoPermissionUser() {
		return noPermissionUser;
	}

	public void logout() {
		if (m_casRestClient != null)
			m_casRestClient.logout();
	}

	public PrepareCASObjects() {
		cleanTestData();
		createAllAccessObjects();
	}

	public void init(String serverName) {
		this.serverName = serverName;

		createPolicies();
		createUserGroups();
		createUsers();
	}

	public void cleanTestData() {
		// clean policies
		RestSearchResultVo<Policy> searchResults = m_casRestClient.searchPolicies("name EQ " + PREFIX + "*");
		List<Policy> results = searchResults.getResults();
		Log.info("Results: " + results.size());
		for (Policy policy : results) {
			m_casRestClient.deletePolicy(policy.getId());
		}

		// clean user groups
		RestSearchResultVo<UserGroup> searchUserGroupResults = m_casRestClient
				.searchUserGroups("name EQ " + PREFIX + "*");
		List<UserGroup> userGroupresults = searchUserGroupResults.getResults();
		Log.info("Results: " + userGroupresults.size());
		for (UserGroup userGroup : userGroupresults) {
			m_casRestClient.deleteUserGroup(userGroup.getId());
		}

		// clean users
		RestSearchResultVo<User> searchUserResults = m_casRestClient.searchUsers("username EQ " + PREFIX + "*");
		List<User> userResults = searchUserResults.getResults();
		Log.info("Results: " + userResults.size());
		for (User user : userResults) {
			m_casRestClient.deleteUser(user.getId());
		}
	}

	public void createAllAccessObjects() {
		Log.info("Create all access Policies");
		Policy createAllAccessPolicy = CasTestUtil.generateCreatePolicy(PREFIX + "all-access");
		createAllAccessPolicy.setServiceType(FmsSecurityTestConstants.SERVICE_TYPE); // same
																						// serviceType
		createAllAccessPolicy.setServerNames(null);
		List<Statement> statements = new ArrayList<>();
		Statement statement = new Statement();
		statement.setEffect(Effect.allow);
		List<String> actions = new ArrayList<>();
		actions.add("*");
		statement.setActions(actions);
		List<String> resources = new ArrayList<>();
		resources.add("*");
		statement.setResources(resources);
		statements.add(statement);
		createAllAccessPolicy.setStatements(statements);
		allAccessPolicy = CasTestUtil.validPolicyCreation(m_casRestClient, createAllAccessPolicy, null);

		Log.info("Create UserGroup with all access policy");
		UserGroup createUserGroup = CasTestUtil.generateCreateUserGroup(PREFIX + "AllAccessPolicy");
		List<String> policyIds = new ArrayList<>();
		policyIds.add(allAccessPolicy.getId());
		createUserGroup.setPolicyIds(policyIds);
		allAccessUserGroup = CasTestUtil.validUserGroupCreation(m_casRestClient, createUserGroup, null);

		Log.info("Update admin with all access permissions");
		User adminUser = m_casRestClient.getUserInfo();
		List<String> groupIds = new ArrayList<>();
		groupIds.add(allAccessUserGroup.getId());
		if (adminUser.getGroupSummaries() != null) {
			// keep existing groupIds
			for (Summary groupSummary : adminUser.getGroupSummaries()) {
				groupIds.add(groupSummary.getId());
			}
		}
		adminUser.setGroupIds(groupIds);
		m_casRestClient.updateUser(adminUser);
	}

	public void createPolicies() {
		Log.info("Create allow Policy");
		Policy createPolicy = CasTestUtil.generateCreatePolicy(PREFIX + "AllowRegion");
		createPolicy.setServiceType(FmsSecurityTestConstants.SERVICE_TYPE);
		List<String> serverNames = new ArrayList<>();
		serverNames.add(serverName);
		createPolicy.setServerNames(serverNames);
		List<Statement> statements = new ArrayList<>();
		Statement statement = new Statement();
		statement.setEffect(Effect.allow);
		List<String> actions = new ArrayList<>();
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_DEVICES_ID_REBOOT);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_DEVICES_REBOOT);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_FIRMWAREUPDATES_SINGLE_DEVICE_EXECUTE);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_FIRMWAREUPDATES_MULTIPLE_DEVICES_EXECUTE);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_BULKOPERATIONS_CANCEL);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_BULKOPERATIONS_ID_CANCEL);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_BULKOPERATIONS_PAUSE);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_BULKOPERATIONS_ID_PAUSE);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_BULKOPERATIONS_RESUME);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_BULKOPERATIONS_ID_RESUME);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_BULKOPERATIONS_RETRY);
		actions.add(FmsSecurityTestConstants.ActionConstants.POST_BULKOPERATIONS_ID_RETRY);
		statement.setActions(actions);
		List<String> resources = new ArrayList<>();
		// resources.add("Region:" + FmsSecurityTestConstants.REGION_NAME);
		resources.add("*");
		statement.setResources(resources);
		statements.add(statement);
		createPolicy.setStatements(statements);
		allowRegionPolicy = CasTestUtil.validPolicyCreation(m_casRestClient, createPolicy, null);

		Log.info("Create deny Policy");
		createPolicy = CasTestUtil.generateCreatePolicy(PREFIX + "DenyRegionCmts");
		createPolicy.setServiceType(FmsSecurityTestConstants.SERVICE_TYPE);
		createPolicy.setServerNames(serverNames);
		statement.setEffect(Effect.deny);
		resources = new ArrayList<>();
		resources.add("Region:" + FmsSecurityTestConstants.REGION_NAME + ":CMTS:" + FmsSecurityTestConstants.CMTS_NAME);
		statement.setResources(resources);
		createPolicy.setStatements(statements);
		denyRegionCmtsPolicy = CasTestUtil.validPolicyCreation(m_casRestClient, createPolicy, null);

		Log.info("Create deny Policy");
		createPolicy = CasTestUtil.generateCreatePolicy(PREFIX + "ReadOnly");
		createPolicy.setServiceType(FmsSecurityTestConstants.SERVICE_TYPE);
		createPolicy.setServerNames(serverNames);
		statement.setEffect(Effect.allow);
		actions.clear();
		actions.add("Get*"); // for UI to display all objects
		statement.setActions(actions);
		resources = new ArrayList<>();
		resources.add("*");
		statement.setResources(resources);
		createPolicy.setStatements(statements);
		readOnlyPolicy = CasTestUtil.validPolicyCreation(m_casRestClient, createPolicy, null);

		// PolicyVo createServiceWidePolicy2 =
		// CasTestUtil.generateCreatePolicy(PREFIX + testName.getMethodName() +
		// "4");
		// createServiceWidePolicy.setServerNames(null); // different
		// serviceType
		// createPolicy.setStatements(statements);
		// PolicyVo createdServiceWidePolicy2 =
		// CasTestUtil.validPolicyCreation(m_casRestClient,
		// createServiceWidePolicy2, null);
	}

	public void createUserGroups() {
		Log.info("Create UserGroup with policyIds");
		UserGroup createUserGroup = CasTestUtil.generateCreateUserGroup(PREFIX + "AllowGroup");
		List<String> policyIds = new ArrayList<>();
		policyIds.add(allowRegionPolicy.getId());
		createUserGroup.setPolicyIds(policyIds);
		allowPoliciesUserGroup = CasTestUtil.validUserGroupCreation(m_casRestClient, createUserGroup, null);
		createUserGroup = CasTestUtil.generateCreateUserGroup(PREFIX + "DenyGroup");
		policyIds.clear();
		policyIds.add(denyRegionCmtsPolicy.getId());
		createUserGroup.setPolicyIds(policyIds);
		denyPoliciesUserGroup = CasTestUtil.validUserGroupCreation(m_casRestClient, createUserGroup, null);

		Log.info("Create UserGroup with read only policyIds");
		createUserGroup = CasTestUtil.generateCreateUserGroup(PREFIX + "ReadOnlyGroup");
		policyIds.clear();
		policyIds.add(readOnlyPolicy.getId());
		createUserGroup.setPolicyIds(policyIds);
		readOnlyUserGroup = CasTestUtil.validUserGroupCreation(m_casRestClient, createUserGroup, null);

		Log.info("Create UserGroup without policyIds");
		createUserGroup = CasTestUtil.generateCreateUserGroup(PREFIX + "NoPoliciesGroup");
		noPoliciesUserGroup = CasTestUtil.validUserGroupCreation(m_casRestClient, createUserGroup, null);
	}

	public void createUsers() {
		Log.info("Create User with groupIds");
		User createUser = CasTestUtil.generateCreateUser(PREFIX + "PermissionUser");
		createUser.setPassword(FmsSecurityTestConstants.USER_PASSWORD);
		List<String> groupIds = new ArrayList<>();
		groupIds.add(allowPoliciesUserGroup.getId());
		groupIds.add(denyPoliciesUserGroup.getId());
		// groupIds.add(readOnlyUserGroup.getId()); // for manual assistant test
		createUser.setGroupIds(groupIds);
		permissionUser = CasTestUtil.validUserCreation(m_casRestClient, createUser, null);

		Log.info("Create User without groupIds");
		createUser = CasTestUtil.generateCreateUser(PREFIX + "NoPermissionUser");
		createUser.setPassword(FmsSecurityTestConstants.USER_PASSWORD);
		groupIds.clear();
		groupIds.add(noPoliciesUserGroup.getId());
		// groupIds.add(readOnlyUserGroup.getId()); // for manual assistant test
		createUser.setGroupIds(groupIds);
		noPermissionUser = CasTestUtil.validUserCreation(m_casRestClient, createUser, null);
	}

	public void updatePermissionPolicy(List<String> actions) {
		allowRegionPolicy = m_casRestClient.getPolicy(allowRegionPolicy.getId());
		allowRegionPolicy.getStatements().get(0).setActions(actions);
		m_casRestClient.updatePolicy(allowRegionPolicy);
	}
}
