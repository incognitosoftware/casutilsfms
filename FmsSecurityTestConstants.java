package com.incognito.utils.fms.security;

public class FmsSecurityTestConstants {
	public static final String SERVICE_TYPE = "Firmware Management Service";
	public static final String REGION_NAME = "RegionName";
	public static final String CMTS_NAME = "CmtsName";

	public static final String USER_PASSWORD = "testFMS-786";

	public static class ActionConstants {
		// devices
		public static final String GET_DEVICES = "GetDevices";
		public static final String GET_DEVICES_ID = "GetSingleDevices";
		public static final String POST_DEVICES_REBOOT = "PostDevicesMultipleReboot";
		public static final String POST_DEVICES_ID_REBOOT = "PostDevicesSingleReboot";

		// firmware updates
		public static final String POST_FIRMWAREUPDATES_SINGLE_DEVICE_EXECUTE = "PostFirmwareupdatesSingleDeviceExecute";
		public static final String POST_FIRMWAREUPDATES_MULTIPLE_DEVICES_EXECUTE = "PostFirmwareupdatesMultipleDevicesExecute";

		// bulk operations
		public static final String POST_BULKOPERATIONS_ID_PAUSE = "PostBulkoperationsSinglePause";
		public static final String POST_BULKOPERATIONS_PAUSE = "PostBulkoperationsPause";
		public static final String POST_BULKOPERATIONS_ID_RESUME = "PostBulkoperationsSingleResume";
		public static final String POST_BULKOPERATIONS_RESUME = "PostBulkoperationsResume";
		public static final String POST_BULKOPERATIONS_ID_CANCEL = "PostBulkoperationsSingleCancel";
		public static final String POST_BULKOPERATIONS_CANCEL = "PostBulkoperationsCancel";
		public static final String POST_BULKOPERATIONS_ID_RETRY = "PostBulkoperationsSingleRetry";
		public static final String POST_BULKOPERATIONS_RETRY = "PostBulkoperationsRetry";
	}
}
