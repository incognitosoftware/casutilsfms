package com.incognito.utils.fms.security;

import com.incognito.rest.common.vo.RestSearchResultVo;
import com.incognito.restclient.RestClient;
import com.incognito.restclient.RestCriteriaBuilder;
import com.incognito.restclient.RestManager;
import com.incognito.utils.fms.objects.*;
import com.sun.jersey.api.client.GenericType;

import java.util.Map;

public class FmsRestClient extends RestClient {
	private static final char RESOURCE_SEPARATOR = '/';

	// Devices
	private static final String DEVICES_PATH = "devices";
	private static final String REBOOT_PATH = "reboot";

	// Firmware updates
	private static final String FIRMWARE_UPDATES_PATH = "firmwareupdates";
	private static final String EXECUTE_PATH = "execute";

	// Bulk Operations
	private static final String BULK_OPERATIONS_PATH = "bulkoperations";
	private static final String PAUSE_PATH = "pause";
	private static final String RESUME_PATH = "resume";
	private static final String CANCEL_PATH = "cancel";
	private static final String RETRY_PATH = "retry";

	// Service Info
	private static final String SERVICEINFO_PATH = "serviceinfo";

	public FmsRestClient(RestManager restManager) {
		m_restManager = restManager;
	}

	// ---------------------- private methods -----------------
	private static String generateDevicesPath(String deviceId) {
		return DEVICES_PATH + RESOURCE_SEPARATOR + deviceId;
	}

	private static String generateFirmwareUpdatePath(String firmwareUpdateId) {
		return FIRMWARE_UPDATES_PATH + RESOURCE_SEPARATOR + firmwareUpdateId;
	}

	private static String generateBulkOperationPath(String bulkOperationId) {
		return BULK_OPERATIONS_PATH + RESOURCE_SEPARATOR + bulkOperationId;
	}

	// --------------------- public methods ------------------
	public ServiceInfo getServiceInfo() {
		return m_restManager.get(SERVICEINFO_PATH, ServiceInfo.class, null);
	}

	// ---------------------- Devices ------------------------
	public Device getDevice(String deviceId) {
		return m_restManager.get(generateDevicesPath(deviceId), Device.class, null);
	}

	public RestSearchResultVo<Device> searchDevices(String query) {
		return searchDevices(query, 0, "", false, false);
	}

	public RestSearchResultVo<Device> searchDevices(String query, int pageSize, String orderBy, boolean ascending,
			boolean caseSensitive) {
		RestCriteriaBuilder queryStringBuilder = new RestCriteriaBuilder(query);
		if (orderBy != null)
			queryStringBuilder.setOrderBy(orderBy);
		if (pageSize > 0)
			queryStringBuilder.setPageSize(pageSize);
		if (!ascending)
			queryStringBuilder.setAscending(ascending);

		Map<String, Object> queryStrings = queryStringBuilder.build();
		queryStrings.put("caseSensitive", caseSensitive);

		return m_restManager.search(DEVICES_PATH, new GenericType<RestSearchResultVo<Device>>() {
		}, queryStrings);
	}

	public void rebootDevice(String deviceId, Reboot reboot) {
		m_restManager.postNoResponse(generateDevicesPath(deviceId) + RESOURCE_SEPARATOR + REBOOT_PATH, reboot);
	}

	public void rebootDevices(String query, Reboot reboot) {
		m_restManager.postNoResponse(DEVICES_PATH + RESOURCE_SEPARATOR + REBOOT_PATH,
				new RestCriteriaBuilder(query).build(), reboot);
	}

	// ---------------------- Firmware Updates ---------------
	public BulkOperation executeFirmwareUpdate(String firmwareUpdateId, ExecuteFirmwareUpdateBody executeFirmwareUpdate) {
		return m_restManager.post(generateFirmwareUpdatePath(firmwareUpdateId) + RESOURCE_SEPARATOR + EXECUTE_PATH,
				executeFirmwareUpdate, BulkOperation.class);
	}

	public RestSearchResultVo<FirmwareUpdate> searchFirmwareUpdates(String query) {
		return searchFirmwareUpdates(query, 0, "", false, false);
	}

	public RestSearchResultVo<FirmwareUpdate> searchFirmwareUpdates(String query, int pageSize, String orderBy,
			boolean ascending, boolean caseSensitive) {
		RestCriteriaBuilder queryStringBuilder = new RestCriteriaBuilder(query);
		if (orderBy != null)
			queryStringBuilder.setOrderBy(orderBy);
		if (pageSize > 0)
			queryStringBuilder.setPageSize(pageSize);
		if (!ascending)
			queryStringBuilder.setAscending(ascending);

		Map<String, Object> queryStrings = queryStringBuilder.build();
		queryStrings.put("caseSensitive", caseSensitive);

		return m_restManager.search(FIRMWARE_UPDATES_PATH, new GenericType<RestSearchResultVo<FirmwareUpdate>>() {
		}, queryStrings);
	}

	// ---------------------- Bulk Operations --------------
	public BulkOperationActionResponse pauseBulkOperation(String bulkOperationId) {
		return m_restManager.post(generateBulkOperationPath(bulkOperationId) + RESOURCE_SEPARATOR + PAUSE_PATH, null,
				BulkOperationActionResponse.class);
	}

	public BulkOperationActionResponse pauseBulkOperations(String query) {
		return m_restManager.post(BULK_OPERATIONS_PATH + RESOURCE_SEPARATOR + PAUSE_PATH,
				new RestCriteriaBuilder(query).build(), BulkOperationActionResponse.class);
	}

	public BulkOperationActionResponse resumeBulkOperation(String bulkOperationId) {
		return m_restManager.post(generateBulkOperationPath(bulkOperationId) + RESOURCE_SEPARATOR + RESUME_PATH, null,
				BulkOperationActionResponse.class);
	}

	public BulkOperationActionResponse resumeBulkOperations(String query) {
		return m_restManager.post(BULK_OPERATIONS_PATH + RESOURCE_SEPARATOR + RESUME_PATH,
				new RestCriteriaBuilder(query).build(), BulkOperationActionResponse.class);
	}

	public BulkOperationActionResponse cancelBulkOperation(String bulkOperationId) {
		return m_restManager.post(generateBulkOperationPath(bulkOperationId) + RESOURCE_SEPARATOR + CANCEL_PATH, null,
				BulkOperationActionResponse.class);
	}

	public BulkOperationActionResponse cancelBulkOperations(String query) {
		return m_restManager.post(BULK_OPERATIONS_PATH + RESOURCE_SEPARATOR + CANCEL_PATH,
				new RestCriteriaBuilder(query).build(), BulkOperationActionResponse.class);
	}

	public BulkOperationActionResponse retryBulkOperation(String bulkOperationId) {
		return m_restManager.post(generateBulkOperationPath(bulkOperationId) + RESOURCE_SEPARATOR + RETRY_PATH, null,
				BulkOperationActionResponse.class);
	}

	public BulkOperationActionResponse retryBulkOperations(String query) {
		return m_restManager.post(BULK_OPERATIONS_PATH + RESOURCE_SEPARATOR + RETRY_PATH,
				new RestCriteriaBuilder(query).build(), BulkOperationActionResponse.class);
	}
}
