package com.incognito.utils.fms.security;

import static org.junit.Assert.assertNotNull;

import com.incognito.eco.cas.restclient.CasRestClient;
import com.incognito.eco.cas.restvo.UserLoginResponse;
import com.incognito.utils.fms.config.RESTClientConfig;

public class EcoTestManager {
	private static EcoTestManager instance;

	private String fmsHost;
	private int fmsPort;
	private String casHost;
	private String casUserName;
	private String casUserPassword;

	private CasRestClient casRestClient;
	private FmsRestClient fmsRestClient;

	private String cleanData;

	public EcoTestManager() {
		init();
	}

	public static EcoTestManager getEcoTestManager() {
		if (instance == null) {
			instance = new EcoTestManager();
		}
		return instance;
	}

	public void init() {
		// get test properties
		fmsHost = RESTClientConfig.getFmsHostname();
		fmsPort = RESTClientConfig.getFmsPort();
		// CAS and CSS on same box
		casHost = RESTClientConfig.getCssHostname();
		casUserName = RESTClientConfig.getCasUsername();
		casUserPassword = RESTClientConfig.getCasPassword();
		cleanData = RESTClientConfig.getCleanData();
	}

	// --------------------- methods ---------------
	public String getCasHost() {
		return casHost;
	}

	public void setCasHost(String casHost) {
		this.casHost = casHost;
	}

	public String getCasUserName() {
		return casUserName;
	}

	public void setCasUserName(String casUserName) {
		this.casUserName = casUserName;
	}

	public String getCasUserPassword() {
		return casUserPassword;
	}

	public void setCasUserPassword(String casUserPassword) {
		this.casUserPassword = casUserPassword;
	}

	public String getFmsHost() {
		return fmsHost;
	}

	public void setFmsHost(String fmsHost) {
		this.fmsHost = fmsHost;
	}

	public int getFmsPort() {
		return fmsPort;
	}

	public void setFmsPort(int fmsPort) {
		this.fmsPort = fmsPort;
	}

	public String getCleanData() {
		return cleanData;
	}

	public void setCleanData(String cleanData) {
		this.cleanData = cleanData;
	}

	// -------------- CAS -------------------------
	public CasRestClient loginCAS() {
		if (casRestClient == null) {
			casRestClient = CasTestUtil.connectPublic(casHost);
		}

		UserLoginResponse loginResp = CasTestUtil.login(casRestClient, casUserName, casUserPassword);
		assertNotNull(loginResp.getAuthorization());
		return casRestClient;
	}

	public void logout() {
		if (casRestClient != null) {
			casRestClient.logout();
		}
	}

	public CasRestClient getCasRestClient() {
		// login CAS
		loginCAS();
		return casRestClient;
	}

	public void setCasRestClient(CasRestClient casRestClient) {
		this.casRestClient = casRestClient;
	}

	// -------------- FMS -----------------------
	public void loginFMS() {
		if (fmsRestClient == null) {
			fmsRestClient = FmsTestUtil.connect(fmsHost, fmsPort);
		}
		UserLoginResponse loginResp = FmsTestUtil.login(fmsRestClient, casHost, casUserName, casUserPassword);
		assertNotNull(loginResp.getAuthorization());
	}

	public FmsRestClient getFmsRestClient() {
		loginFMS();
		return fmsRestClient;
	}

	public void setFmsRestClient(FmsRestClient fmsRestClient) {
		this.fmsRestClient = fmsRestClient;
	}
}
